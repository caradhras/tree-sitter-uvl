# tree-sitter-uvl
[UVL](https://github.com/Universal-Variability-Language) grammar for [tree-sitter](https://github.com/tree-sitter/tree-sitter). Mainly used in the [UVLS](https://codeberg.org/caradhras/uvls.git) language server,  
so it may not be the most ergonomic grammar since it encodes error states.

